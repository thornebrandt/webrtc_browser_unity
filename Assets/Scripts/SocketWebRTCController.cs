using System.Collections;
using System.Collections.Generic;
using Firesplash.UnityAssets.SocketIO;
using Microsoft.MixedReality.WebRTC.Unity;
using Newtonsoft.Json;
using Unity.WebRTC;
using Unity.WebRTC.Samples;
using UnityEngine;
using UnityEngine.UI;

//no way

namespace gj {
    public class SocketWebRTCController : MonoBehaviour {
        private int width = 400;
        private int height = 300;
        private int bufferSize = 1024;
        public SocketIOCommunicator sioCom;
        public Camera cam;
        public GameObject displayGameObject;
        public GameObject displayUnlitPlane;
        public RawImage displayImage;
        private VideoStreamTrack camTrack;
        private VideoStreamTrack incomingTrack;
        private RTCIceServer[] iceServers;
        private Dictionary<string, Client> clients;
        private bool videoUpdateStarted;
        private List<RTCRtpSender> camSenders;
        private string id;
        private Renderer rend;
        private Renderer rendUnlit;

        void Start () {
            assignRenderers ();
            WebRTC.Initialize (EncoderType.Software);
            clients = new Dictionary<string, Client> ();
            setupCameraTrack ();
            socketHandler ();
            sioCom.Instance.Connect ();
            camSenders = new List<RTCRtpSender> ();
        }

        private void OnDestroy () {
            WebRTC.Dispose ();
        }

        private void assignRenderers () {
            if (displayGameObject != null) {
                rend = displayGameObject.GetComponent<Renderer> ();
            }
            if (displayUnlitPlane != null) {
                rendUnlit = displayUnlitPlane.GetComponent<Renderer> ();
            }
        }

        private void setupCameraTrack () {
            camTrack = cam.CaptureStreamTrack (width, height, bufferSize);
        }

        private void socketHandler () {
            sioCom.Instance.On ("connect", (string data) => {
                Debug.Log ("Socket.io successfully connected.");
            });

            sioCom.Instance.On ("introduction", (string payloadString) => {
                SocketPayload payload = JsonConvert.DeserializeObject<SocketPayload> (payloadString);
                id = payload.id;
                List<string> urls = new List<string> ();
                foreach (IceServer iceServer in payload.iceServers) {
                    urls.Add (iceServer.url);
                }
                iceServers = new [] {
                    new RTCIceServer {
                    urls = urls.ToArray (),
                    }
                };

                foreach (string client_id in payload.clients) {
                    if (client_id != id) {
                        addClient (client_id);
                        StartCoroutine (callUser (client_id));
                    }
                }
            });

            sioCom.Instance.On ("newUserConnected", (string payloadString) => {
                SocketPayload payload = JsonConvert.DeserializeObject<SocketPayload> (payloadString);
                bool alreadyHasUser = false;

                foreach (KeyValuePair<string, Client> _client in clients) {
                    Client client = _client.Value;
                    if (client.id == payload.id) {
                        alreadyHasUser = true;
                        break;
                    }
                }

                if (payload.id != id && !alreadyHasUser) {
                    addClient (payload.id);
                }
            });

            sioCom.Instance.On ("userDisconnected", (string payloadString) => {
                SocketPayload payload = JsonConvert.DeserializeObject<SocketPayload> (payloadString);
                if (payload.id != id) {
                    //removeClientVideo.
                    clients.Remove (payload.id);
                }
            });

            sioCom.Instance.On ("call-made", (string payloadString) => {
                SocketPayload payload = JsonConvert.DeserializeObject<SocketPayload> (payloadString);
                if (clients.ContainsKey (payload.socket)) {
                    Client client = clients[payload.socket];
                    StartCoroutine (onCallMade (client, payload));
                } else {
                    Debug.Log (payload.socket + "not found in " + clients.Count + " clients");
                }
            });

            sioCom.Instance.On ("answer-made", (string payloadString) => {
                SocketPayload payload = JsonConvert.DeserializeObject<SocketPayload> (payloadString);
                if (clients.ContainsKey (payload.socket)) {
                    Client client = clients[payload.socket];
                    StartCoroutine (onAnswerMade (client, payload));
                }
            });

            sioCom.Instance.On ("iceCandidateFound", (string payloadString) => {
                SocketPayload payload = JsonConvert.DeserializeObject<SocketPayload> (payloadString);
                Client client = clients[payload.socket];
                RTCIceCandidate candidate = new RTCIceCandidate (payload.candidate);
                client.peerConnection.AddIceCandidate (candidate);
            });

        }

        private void addClient (string client_id) {
            clients.Add (client_id, new Client ());
            clients[client_id].id = client_id;
            RTCPeerConnection peerConnection = createPeerConnection (client_id);
            clients[client_id].peerConnection = peerConnection;
            clients[client_id].isAlreadyCalling = false;
        }

        private RTCPeerConnection createPeerConnection (string client_id) {
            RTCConfiguration config = new RTCConfiguration ();
            config.bundlePolicy = RTCBundlePolicy.BundlePolicyMaxBundle;
            config.iceServers = iceServers;
            RTCPeerConnection pc = new RTCPeerConnection (ref config);

            pc.OnTrack = e => {
                if (rend != null) {
                    //perhaps take off check for isDecoderInitialized.
                    if (e.Track is VideoStreamTrack videoStreamTrack && !videoStreamTrack.IsDecoderInitialized) {
                        AddVideoTrackToRenderTexture (videoStreamTrack);
                    }
                }
            };

            pc.OnIceCandidate = candidate => {
                if (candidate != null) {
                    SocketPayload icePayload = new SocketPayload ();
                    icePayload.to = client_id;
                    string candidateString = JsonConvert.SerializeObject (candidate);
                    RTCIceCandidateInit iceCandidateInfo = JsonConvert.DeserializeObject<RTCIceCandidateInit> (candidateString);
                    icePayload.candidate = iceCandidateInfo;
                    string icePayloadString = JsonConvert.SerializeObject (icePayload);
                    sioCom.Instance.Emit ("addIceCandidate", icePayloadString);
                }
            };

            addTracksToPeerConnection (camTrack, pc);
            clients[client_id].peerConnection = pc;
            return pc;
        }

        private void AddVideoTrackToRenderTexture (VideoStreamTrack videoStreamTrack) {
            Texture incomingStreamTexture = videoStreamTrack.InitializeReceiver (width, height);
            displayImage.texture = incomingStreamTexture;
            rend.sharedMaterial.SetTexture ("_BaseColorMap", incomingStreamTexture);
            if (rendUnlit != null) {
                rendUnlit.sharedMaterial.SetTexture ("_UnlitColorMap", incomingStreamTexture);
            }
        }

        private void addTracksToPeerConnection (VideoStreamTrack camTrack, RTCPeerConnection pc) {
            pc.AddTrack (camTrack);
            if (!videoUpdateStarted) {
                StartCoroutine (WebRTC.Update ());
                videoUpdateStarted = true;
            }
        }

        private IEnumerator callUser (string client_id) {
            if (clients.ContainsKey (client_id)) {
                Client client = clients[client_id];
                RTCPeerConnection pc = clients[client_id].peerConnection;
                var offer = pc.CreateOffer ();
                yield return offer;
                if (offer.IsError) {
                    Debug.Log ("something went wrong calling user " + client_id + " " + offer.Error.message);
                } else {
                    if (pc.SignalingState != RTCSignalingState.Stable) {
                        Debug.LogError ($"{client_id} signaling state is not stable.");
                        yield break;
                    }
                }

                RTCSessionDescription offerDesc = offer.Desc;
                var operation = pc.SetLocalDescription (ref offerDesc);
                yield return operation;
                if (operation.IsError) {
                    Debug.Log ("something went wrong setting local description for " + client.id + " " + operation.Error.message);
                }

                SocketPayload offerPayload = new SocketPayload ();
                offerPayload.offer = offer.Desc;
                offerPayload.to = client_id;
                string offerPayloadString = JsonConvert.SerializeObject (offerPayload);
                sioCom.Instance.Emit ("call-user", offerPayloadString);
            }
        }

        private IEnumerator onCallMade (Client client, SocketPayload payload) {
            RTCPeerConnection pc = client.peerConnection;
            var offer = pc.SetRemoteDescription (ref payload.offer);
            yield return offer;
            if (offer.IsError) {
                Debug.Log ("something went wrong onCallMade setting remote description for " + client.id + " " + offer.Error.message);
                yield break;
            }

            var answer = pc.CreateAnswer ();
            yield return answer;

            if (answer.IsError) {
                Debug.Log ("something went wrong creating answer for " + client.id + " " + answer.Error.message);
            }

            RTCSessionDescription answerDesc = answer.Desc;
            var operation = pc.SetLocalDescription (ref answerDesc);
            yield return operation;
            if (operation.IsError) {
                Debug.Log ("something went wrong setting local description for " + client.id + " " + operation.Error.message);
            }

            SocketPayload answerPayload = new SocketPayload ();
            answerPayload.answer = answerDesc;
            answerPayload.to = payload.socket;

            string answerPayloadString = JsonConvert.SerializeObject (answerPayload);
            sioCom.Instance.Emit ("make-answer", answerPayloadString);
        }

        private IEnumerator onAnswerMade (Client client, SocketPayload payload) {
            RTCPeerConnection pc = client.peerConnection;
            var description = pc.SetRemoteDescription (ref payload.answer);
            yield return description;
            if (description.IsError) {
                Debug.Log ("something went wrong creating remote description for " + client.id + " " + description.Error.message);
            }
            if (!client.isAlreadyCalling) {
                StartCoroutine (callUser (payload.socket));
                client.isAlreadyCalling = true;
            }
        }
    }
}