// express server

const express = require("express");
const app = express();
app.use(express.static("public"));
const http = require("http").createServer(app);
const port = process.env.PORT || 3000;
const server = app.listen(port);
console.log("Server is running on http://localhost:" + port);



// socket.io
const io = require("socket.io")().listen(server);


// DO NOT USE in production. 

// we are using google's public stun servers for testing purposes.
// eventually we will need to set up a TURN service.
// https://github.com/coturn/coturn
// or use an existing service such as Twilio. 


let iceServers = [
    { url: "stun:stun.l.google.com:19302" },
    { url: "stun:stun1.l.google.com:19302" },
    { url: "stun:stun2.l.google.com:19302" },
    { url: "stun:stun3.l.google.com:19302" },
    { url: "stun:stun4.l.google.com:19302" },
];


let clients = {};

function main() {
    setupSocketServer();

    // setInterval(function() {
    //     io.sockets.em
    // }, 10);
}

main();

function setupSocketServer() {
    io.on("connection", (client) => {
        console.log("User " + client.id + " connected");

        clients[client.id] = {
            id: client.id
        };

        client.emit(
            "introduction",
            {
                id: client.id,
                iceServers,
                clients: Object.keys(clients)
            }
        );

        io.sockets.emit(
            "newUserConnected",
            {
                id: client.id,
                clients: Object.keys(clients)
            }
        );

        client.on("disconnect", () => {
            delete clients[client.id];
            io.sockets.emit(
                "userDisconnected",
                {
                    id: client.id,
                    clients: Object.keys(clients)
                }
            );

            console.log(
                "User " + client.id + " disconnected"
            );
        });


        //webrtc communications
        client.on("call-user", (data) => {
            console.log("Server forwarding call from " + client.id + " to " + data.to);
            client.to(data.to).emit("call-made", {
                offer: data.offer,
                socket: client.id
            });
        });

        client.on("make-answer", (data) => {
            client.to(data.to).emit("answer-made", {
                socket: client.id,
                answer: data.answer
            });
        });

        client.on("addIceCandidate", (data) => {
            client.to(data.to).emit("iceCandidateFound", {
                socket: client.id,
                candidate: data.candidate
            });
        });
    });
}