using System.Collections;
using System.Collections.Generic;
using Unity.WebRTC;
using UnityEngine;

namespace gj {
    public class SocketPayload {
        //reusable nullable class for serializing / deserializing data via json. 
        public List<string> clients;
        public string id;
        public string socket; //same as id. 
        public string to;
        public List<IceServer> iceServers;
        public RTCSessionDescription offer;
        public RTCSessionDescription answer;
        public RTCIceCandidateInit candidate;
        //todo get ice candidate type. 
    }
}