# WebRTC for Browser and Unity Example
## Automatic Video Streaming Between Unity HDRP and Browser using WebRTC and Socket.IO 

---

### QuickStart 

Express Server is located in `./node-socket-server`   

Install Dependencies and Start Server with `npm start`

Open `localhost:3000` in  browser 

Start Unity



After a few moments, you should see your webcam on animated shapes, and the camera from unity streaming back to browser. 


![img](screenshot.png)


---

### Requirements 

* [Unity HDRP](https://unity3d.com/beta/2021.1a) > 2020
* [Node.js](https://nodejs.org/en/)

---

### Installation 


Clone the repo. 

```bash
git clone https://gitlab.com/thornebrandt/webrtc_browser_unity.git
```



Install node modules for socket.io and express server. 

( Express Server is located in `./node-socket-server` of root folder. )


```bash
cd node-socket-server
npm install

```

Start the express server. ( while still in `node-socket-server` folder ) 


```bash
npm start
```


That's it!   Open [localhost:3000](localhost:3000) and you should see a video of yourself. 
Start *Assets > Scenes > WebRTC* in Unity Editor. 


### Editing  

*Assets > Scenes > WebRTC* to edit displays.  

Within this scene, look at the *SocketWebRTCController* gameObject and the [SocketWebRTCController.cs](https://gitlab.com/thornebrandt/webrtc_browser_unity/-/blob/master/Assets/Scripts/SocketWebRTCController.cs) script. 

Edit resolutions, framerates, and bitrates here, as well as what happens after a peer connection receives a video track around *AddVideoTracktoRenderTexture*

A similar controller is handled on the javascript client side [node-socket-server/public/index.js](https://gitlab.com/thornebrandt/webrtc_browser_unity/-/blob/master/node-socket-server/public/index.js)

---


### Secure HTTPS Remote Server 

You need https to open webcam on brwosers. 

You need an ssl certificate to share video streams over https. 

If you have a valid SSL certificate on a remote server. For local https development, keep scrolling. 

1. Check out the [secure_server](https://gitlab.com/thornebrandt/webrtc_browser_unity/-/commits/secure_server) branch. This includes changes that ask node and unity to run in HTTPS. 

2. Change the public client [node-socket-server/public/index.js](https://gitlab.com/thornebrandt/webrtc_browser_unity/-/commit/02e3a8670e30c15bf3468be86964bb1bef6222b3#03f5c8fd1ab10bc049362f1a1cc003caf09df47d) to remote server where the node app is running.

3. If using a port other than 3000, change this in [node-socket-server/server.js](https://gitlab.com/thornebrandt/webrtc_browser_unity/-/commit/02e3a8670e30c15bf3468be86964bb1bef6222b3)

4. In Unity, *Assets > Scenes > WebRTC*  open the  *SocketWebRTCController* gameObject and change both the remote address to the remote address (leave out the https protocol), and make sure the box that says *Secure Connection*  "Using SSL / TLS? Do Not check this box if you are not using a publicly trusted SSL certificate for your server. is checked. 

^ Key word here is "publicly trusted."  This is why Unity isn't going to trust your self signed local SSL certificate.  
(If someone knows how to get Unity to force reading ssl certificates like a browser, let me know.)

---

### Secure HTTPS for local development 

1. Check out the [secure_server](https://gitlab.com/thornebrandt/webrtc_browser_unity/-/commits/secure_server) branch. This will point the ports in the right direction for https. 

2. Install [openssl](https://www.openssl.org/)  either using PERL, but if you have linux or [wsl](https://docs.microsoft.com/en-us/windows/wsl/install)  it might already be installed.   Check by just typing `openssl`

3. Create a `certs` folder inside of `node-socket-server`   `node-socket-server/server.js` is pointing to files that will be generatted here. 

4. Generate root ssl certificate

```
openssl genrsa -des3 -out rootCA.key 2048
```
5. Enter passphrase and store it. 

6. Create root certificate with the generated key. 

```
openssl req -x509 -new -nodes -key rootCA.key -sha256 -days 1460 -out rootCA.pem
```

7. Fill out all the information to your liking.

8. Trust the root ssl certificate. You need elevated access for this.  Open powershell as administrator, navigate to the `node-socket-server/certs` folder 

```
certutil -addstore -f "ROOT" rootCA.pem
```
You should get an added successfully command. 

9.  You can verify that this was added to root certificate authorities in Chrome by navigating to `Settings → Privacy and Security → Security → Manage Certificates → Trusted Root Certification Authorities.`

10. Create a `server.csr.cnf` file in the `certs` folder with the following. 

```
[req]
default_bits = 2048
prompt = no
default_md = sha256
distinguished_name = dn

[dn]
C=US
ST=Illinois
L=Chicago
O=Yada Yada
OU=Yada Yada
emailAddress=yadayada@whatever.com
CN = localhost
```

11. Create a `v3.ext` file with the following: 

```
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names
[alt_names]
DNS.1 = localhost
```


12. Use these files to create a signing request. 

```
openssl req -new -sha256 -nodes -out server.csr -newkey rsa:2048 -keyout server.key -config server.csr.cnf
```

13. A `server.key` file has been generated. 

14. Issue a certificate.   Remember the passphrase you wrote down earlier? Get that ready because it will ask for it. 

```
openssl x509 -req -in server.csr -CA rootCA.pem -CAkey rootCA.key -CAcreateserial -out server.crt -days 500 -sha256 -extfile v3.ext
```
This will generate a `server.crt` file that translated your passphrase.  

`server.js` is already pointed towards these files with   

```
const options = {
    key: fs.readFileSync('certs/server.key'),
    cert: fs.readFileSync('certs/server.crt')
};
```

so you shouldn't have to change anything there.  


16. Assuming you haven't experienced any errors, this branch should be set up to work with `npm start` from the *node-socket-server* folder.    Open up "https://localhost:3000" and you should see a green lock sign which means that Chrome trusts this address. 

Use it the same as you did for the Installation instructions. 

For debugging, there is also a very simple server which you can start with `node test_ssl_server.js
