using System.Collections;
using System.Collections.Generic;
using Unity.WebRTC;
using UnityEngine;

namespace gj {
    public class Client {
        public RTCPeerConnection peerConnection;
        public bool isAlreadyCalling;
        public string id;
    }
}