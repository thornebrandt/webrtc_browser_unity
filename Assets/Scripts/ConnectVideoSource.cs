using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectVideoSource : MonoBehaviour {
    public Microsoft.MixedReality.WebRTC.Unity.VideoRenderer videoRenderer;
    public Microsoft.MixedReality.WebRTC.Unity.WebcamSource webcamSource;

    public void startVideoStream () {
        videoRenderer.StartRendering (webcamSource.Source);
    }

    public void stopVideoStream () {
        videoRenderer.StopRendering (webcamSource.Source);
    }

}