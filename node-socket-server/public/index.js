let socket;
let id;
let clients = {};
const { RTCPeerConnection, RTCSessionDescription } = window;
let iceServerList;

const videoWidth = 400;
const videoHeight = 300;
const videoFrameRate = 30;

let videoElement;
let localMediaStream = null;

let mediaConstraints = {
    video: {
        width: videoWidth,
        height: videoHeight,
        frameRate: videoFrameRate,
    },
};


window.onload = async () => {
    localMediaStream = await getMedia(mediaConstraints);
    createLocalVideoElement();
    initSocketConnection();
};

async function getMedia(_mediaConstraints) {
    let stream = null;
    try {
        stream = await navigator.mediaDevices.getUserMedia(_mediaConstraints);
    } catch (err) {
        console.log("Failed to get user media!");
        console.warn(err);
    }
    return stream;
}


function addTracksToPeerConnection(_stream, _pc) {
    if (_stream == null) {
        console.log("Local User media stream not yet established!");
    } else {
        _stream.getTracks().forEach((track) => {
            _pc.addTrack(track, _stream);
        });
    }
}


function initSocketConnection() {
    socket = io();
    socket.on("connect", () => {
        console.log("socket.io connected");
    });
    socket.on("introduction", (payload) => {
        iceServerList = payload.iceServers;
        id = payload.id;

        for (let i = 0; i < payload.clients.length; i++) {
            if (payload.clients[i] != id) {
                addClient(payload.clients[i]);
                callUser(payload.clients[i]);
            }
        }
    });

    socket.on("newUserConnected", (payload) => {
        let alreadyHasUser = false;
        for (let i = 0; i < Object.keys(clients).length; i++) {
            if (Object.keys(clients)[i] == payload.id) {
                alreadyHasUser = true;
                break;
            }
        }

        if (payload.id != id && !alreadyHasUser) {
            addClient(payload.id);
        }
    });

    socket.on("userDisconnected", (payload) => {
        if (payload.id != id) {
            removeClientVideoElement(payload.id);
            delete clients[payload.id];
        }
    });


    socket.on("call-made", async (data) => {
        data.offer.type = convertUnityType(data.offer.type);

        await clients[data.socket].peerConnection.setRemoteDescription(
            new RTCSessionDescription(data.offer)
        );

        const answer = await clients[data.socket].peerConnection.createAnswer();
        window.sdp = data.offer.sdp;
        await clients[data.socket].peerConnection.setLocalDescription(
            new RTCSessionDescription(answer)
        );

        socket.emit("make-answer", {
            answer,
            to: data.socket,
        });
    });

    socket.on("answer-made", async (data) => {
        if (data.answer.type === "2" || data.answer.type === 2) {
            data.answer.type = "answer";
        }

        await clients[data.socket].peerConnection.setRemoteDescription(
            new RTCSessionDescription(data.answer)
        );

        // preventing double calls. 
        if (!clients[data.socket].isAlreadyCalling) {
            callUser(data.socket);
            clients[data.socket].isAlreadyCalling = true;
        }
    });

    socket.on("iceCandidateFound", (data) => {
        clients[data.socket].peerConnection.addIceCandidate(data.candidate);
    });
}

function addClient(_id) {
    clients[_id] = {};
    let pc = createPeerConnection(_id);
    clients[_id].peerConnection = pc;
    clients[_id].isAlreadyCalling = false;
    createClientMediaElements(_id);

}

function createPeerConnection(_id) {
    let peerConnectionConfiguration;
    peerConnectionConfiguration = { iceServers: iceServerList, bundlePolicy: "max-compat" };
    let pc = new RTCPeerConnection(peerConnectionConfiguration);

    pc.ontrack = function ({ streams: [_remoteStream] }) {
        let videoStream = new MediaStream([_remoteStream.getVideoTracks()[0]]);
        const remoteVideoElement = document.getElementById(_id + "_video");
        if (remoteVideoElement) {
            remoteVideoElement.srcObject = videoStream;
        } else {
            console.warn("No video element found for ID: " + _id);
        }
    }

    pc.onicecandidate = (e) => {
        if (e.candidate) {
            socket.emit("addIceCandidate", {
                candidate: e.candidate,
                to: _id,
            });
        }
    }

    addTracksToPeerConnection(localMediaStream, pc);
    return pc;
}

async function callUser(id) {
    if (clients.hasOwnProperty(id)) {
        const offer = await clients[id].peerConnection.createOffer();
        await clients[id].peerConnection.setLocalDescription(
            new RTCSessionDescription(offer)
        );

        socket.emit("call-user", {
            offer,
            to: id,
        });
    }
}

function disableOutgoingStream() {
    localMediaStream.getVideoTracks().forEach((track) => {
        track.enabled = false;
    });
}

function enableOutgoingStream() {
    localMediaStream.getTracks().forEach((track) => {
        track.enabled = true;
    });
}


function createLocalVideoElement() {
    videoElement = document.createElement("video");
    videoElement.id = "local_video";
    videoElement.autoplay = true;
    videoElement.width = videoWidth;
    videoElement.height = videoHeight;
    let videoStream = new MediaStream([localMediaStream.getVideoTracks()[0]]);
    videoElement.srcObject = videoStream;
    document.body.appendChild(videoElement);
}




function createClientMediaElements(_id) {
    const videoElement = document.createElement("video");
    videoElement.id = _id + "_video";
    videoElement.width = videoWidth;
    videoElement.height = videoHeight;
    videoElement.autoplay = true;
    document.body.appendChild(videoElement);
}


function removeClientVideoElement(_id) {
    let videoEl = document.getElementById(_id + "_video");
    if (videoEl != null) {
        videoEl.remove();
    }
}



function convertUnityType(_type) {
    //dirty hack for unity enums. 
    if (_type === "0" || _type === 0) {
        return "offer";
    }

    if (_type === "2" || _type === 2) {
        return "answer";
    }
    return _type;
}





